from django.conf.urls import url

from . import views

app_name = 'shoutbox'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^shout/$', views.shout, name='shout'),
    url(r'^ip/$', views.ip, name='ip')
]
