from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from requests import get

from .models import Shout


def index(request):
    context = {'shouts': Shout.objects.all()}
    return render(request, 'shoutbox/index.html', context)


def shout(request):
    sender = request.POST['sender']
    message = request.POST['message']
    Shout.objects.create(sender=sender, message=message)
    return HttpResponseRedirect(reverse('shoutbox:index'))


def ip(request):
    return HttpResponse(get('https://httpbin.org/ip'))