from __future__ import unicode_literals

from django.db import models

class Shout(models.Model):
    message = models.CharField(max_length=200)
    sender = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now=True)
